/**
 * 
 */
package com.yandex.disk.rest;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.yandex.disk.rest.json.ApiVersion;
import com.yandex.disk.rest.json.DiskInfo;
import com.yandex.disk.rest.json.Link;
import com.yandex.disk.rest.json.Resource;
import com.yandex.disk.rest.util.ResourcePath;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YandexDiskClient extends RestClient {

    public YandexDiskClient(final Credentials credentials) {
    	super(credentials);
    }

    public YandexDiskClient(final Credentials credentials, final OkHttpClient client) {
        super(credentials, client);
    }

    public YandexDiskClient(final Credentials credentials, final OkHttpClient client, final String serverUrl) {
    	 super(credentials, client, serverUrl);
    }
    
    public static String getParentPath(final String path) {
    	return "disk:" + FilenameUtils.getPath(path);
    }
    
	private static RuntimeException getException(Exception e) {
		// TODO Auto-generated method stub
		return null;
	}

    public OkHttpClient getNativeClient() {
    	return getClient();
    }
    
    public String getClientUri() {
    	return getUrl();
    }
    
    public void addInterceptor(final Interceptor interceptor) {
    	getClient().networkInterceptors().add(interceptor);
    }
    
    public Resource getResourceByPath(final String path) {
    	try {
    		final ResourcesArgs.Builder builder = new ResourcesArgs.Builder();
    		builder.setPath(path);
    		return super.getResources(builder.build());
		} catch (Exception e) {
			throw getException(e);
		}
    }

	public Resource getFolderParent(final ResourcePath path) {
		try {
			final ResourcesArgs.Builder builder = new ResourcesArgs.Builder();
			final String pathParent = getParentPath(path.getPath());
			builder.setPath(pathParent);
			return super.getResources(builder.build());
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public ApiVersion safeGetApiVersion() {
		try {
			return super.getApiVersion();
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public DiskInfo safeGetDiskInfo() {
		try {
			return super.getDiskInfo();
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public Resource safeGetResources(ResourcesArgs args) {
		try {
			return super.getResources(args);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public void safeDownloadFile(String path, DownloadListener downloadListener) {
		try {
			super.downloadFile(path, downloadListener);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public void safeUploadFile(Link link, boolean resumeUpload, File localSource, ProgressListener progressListener) {
		try {
			super.uploadFile(link, resumeUpload, localSource, progressListener);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public Link safeDelete(String path, boolean permanently) {
		try {
			return super.delete(path, permanently);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public Link safeMakeFolder(String path) {
		try {
			return super.makeFolder(path);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public Link safeCopy(String from, String path, boolean overwrite) {
		try {
			return super.copy(from, path, overwrite);
		} catch (Exception e) {
			throw getException(e);
		}
	}

	public Link safeMove(String from, String path, boolean overwrite)  {
		try {
			return super.move(from, path, overwrite);
		} catch (Exception e) {
			throw getException(e);
		}
	}
	
	
}
