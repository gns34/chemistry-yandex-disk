/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.datatype;

import org.apache.chemistry.opencmis.client.api.FolderType;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YaFolderType extends AbstractYaObjectType implements FolderType {

	private static final long serialVersionUID = 1L;

	public YaFolderType() {
		super("dir");
	}

}
