/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.datatype;

import java.util.Collections;
import java.util.List;

import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.Tree;

/**
 * @author sbt-goncharov-ns
 *
 */
public class AbstractYaObjectType extends YaTypeDefinition implements ObjectType {

	private static final long serialVersionUID = 1L;
	
	protected AbstractYaObjectType(String type) {
		super(type);
	}

	@Override
	public boolean isBaseType() {
		return true;
	}

	@Override
	public ObjectType getBaseType() {
		return null;
	}

	@Override
	public ObjectType getParentType() {
		return null;
	}

	@Override
	public ItemIterable<ObjectType> getChildren() {
		return null;
	}

	@Override
	public List<Tree<ObjectType>> getDescendants(int depth) {
		return Collections.emptyList();
	}

}
