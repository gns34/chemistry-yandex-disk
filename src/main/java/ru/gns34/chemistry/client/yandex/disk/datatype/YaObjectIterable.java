package ru.gns34.chemistry.client.yandex.disk.datatype;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ItemIterable;

import com.yandex.disk.rest.json.Resource;

import ru.gns34.chemistry.client.yandex.disk.YandexDiskSessionImplRest;
import ru.gns34.chemistry.client.yandex.disk.data.YandexFile;
import ru.gns34.chemistry.client.yandex.disk.data.YandexFolder;

public class YaObjectIterable implements ItemIterable<CmisObject> {

	private final List<CmisObject> objects;
	
	public YaObjectIterable(YandexDiskSessionImplRest session, List<Resource> list) {
		List<CmisObject> objects = new ArrayList<>(list.size());
		for (Resource resource : list) {
			if ("dir".equals(resource.getType())) {
				objects.add(new YandexFolder(session, resource));
			} else if ("file".equals(resource.getType())) {
				objects.add(new YandexFile(session, resource));
			}
		}
		this.objects = Collections.unmodifiableList(objects);
	}

	@Override
	public ItemIterable<CmisObject> skipTo(long position) {
		return this;
	}

	@Override
	public ItemIterable<CmisObject> getPage() {
		return this;
	}

	@Override
	public ItemIterable<CmisObject> getPage(int maxNumItems) {
		return this;
	}

	@Override
	public Iterator<CmisObject> iterator() {
		return objects.iterator();
	}

	@Override
	public long getPageNumItems() {
		return 1;
	}

	@Override
	public boolean getHasMoreItems() {
		return false;
	}

	@Override
	public long getTotalNumItems() {
		return objects.size();
	}

}
