package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.Collections;
import java.util.List;

import org.apache.chemistry.opencmis.commons.data.AclCapabilities;
import org.apache.chemistry.opencmis.commons.data.ExtensionFeature;
import org.apache.chemistry.opencmis.commons.data.RepositoryCapabilities;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.CmisVersion;

/**
 * @author Гончаров Никита
 *
 */
public class YandexRepositoryInfo extends YandexAllowableActions implements RepositoryInfo {

	private static final long serialVersionUID = 1L;
	private static final String NAME = "Яндекс.Диск";
	
	private String productVersion;
	private String clientUri;

	@Override
	public String getId() {
		return "disk";
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return NAME;
	}

	@Override
	public String getVendorName() {
		return NAME;
	}

	@Override
	public String getProductName() {
		return NAME;
	}

	@Override
	public String getProductVersion() {
		return this.productVersion;
	}

	@Override
	public String getRootFolderId() {
		return "disk:/";
	}

	@Override
	public RepositoryCapabilities getCapabilities() {
		return null;
	}

	@Override
	public AclCapabilities getAclCapabilities() {
		return null;
	}

	@Override
	public String getLatestChangeLogToken() {
		return null;
	}

	@Override
	public String getCmisVersionSupported() {
		return CmisVersion.CMIS_1_0.toString();
	}

	@Override
	public CmisVersion getCmisVersion() {
		return CmisVersion.CMIS_1_0;
	}

	@Override
	public String getThinClientUri() {
		return clientUri;
	}

	@Override
	public Boolean getChangesIncomplete() {
		return null;
	}

	@Override
	public List<BaseTypeId> getChangesOnType() {
		return null;
	}

	@Override
	public String getPrincipalIdAnonymous() {
		return null;
	}

	@Override
	public String getPrincipalIdAnyone() {
		return null;
	}

	@Override
	public List<ExtensionFeature> getExtensionFeatures() {
		return Collections.emptyList();
	}

	/**
	 * @param clientUri the clientUri to set
	 */
	public void setClientUri(String clientUri) {
		this.clientUri = clientUri;
	}

	/**
	 * @param productVersion the productVersion to set
	 */
	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

}
