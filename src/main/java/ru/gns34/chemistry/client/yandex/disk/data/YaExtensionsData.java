/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.Collections;
import java.util.List;

import org.apache.chemistry.opencmis.commons.data.CmisExtensionElement;
import org.apache.chemistry.opencmis.commons.data.ExtensionsData;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YaExtensionsData implements ExtensionsData {

	@Override
	public List<CmisExtensionElement> getExtensions() {
		return Collections.emptyList();
	}

	@Override
	public void setExtensions(List<CmisExtensionElement> extensions) {
	}
}
