/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.datatype;

import org.apache.chemistry.opencmis.commons.definitions.TypeMutability;

import ru.gns34.chemistry.client.yandex.disk.data.YaExtensionsData;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YaTypeMutability extends YaExtensionsData implements TypeMutability {

	private static final YaTypeMutability _THIS = new YaTypeMutability();
	
	public static TypeMutability getInstance() {
		return _THIS;
	}
	
	private YaTypeMutability() {
	}

	@Override
	public Boolean canCreate() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean canUpdate() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean canDelete() {
		return Boolean.TRUE;
	}

}
