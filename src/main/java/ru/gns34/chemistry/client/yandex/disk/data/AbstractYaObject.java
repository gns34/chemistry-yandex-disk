/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.apache.chemistry.opencmis.client.api.Rendition;
import org.apache.chemistry.opencmis.client.api.SecondaryType;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.data.CmisExtensionElement;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.enums.ExtensionLevel;

import com.yandex.disk.rest.json.Resource;
import com.yandex.disk.rest.util.ResourcePath;

/**
 * @author Гончаров Никита
 * @since 1.0
 */
public abstract class AbstractYaObject implements CmisObject {

	private static final String OWNER = "owner";
	private static final char SEPARATOR = ':';

	protected final Resource resource;
	protected long timeRefresh;
	
	protected AbstractYaObject(Resource resource) {
		this.resource = resource;
	}
	
	@Override
	public String getId() {
		ResourcePath path = this.resource.getPath();
		return (path.getPrefix() != null ? path.getPrefix() + SEPARATOR : "") + path.getPath();
	}
	
	@Override
	public String getName() {
		return this.resource.getName();
	}

	@Override
	public List<Property<?>> getProperties() {
		return Collections.emptyList();
	}

	@Override
	public <T> Property<T> getProperty(String id) {
		return null;
	}

	@Override
	public <T> T getPropertyValue(String id) {
		return null;
	}


	@Override
	public String getDescription() {
		return this.resource.getName();
	}

	@Override
	public String getCreatedBy() {
		return OWNER;
	}

	@Override
	public GregorianCalendar getCreationDate() {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(this.resource.getCreated());
		return c;
	}

	@Override
	public String getLastModifiedBy() {
		return OWNER;
	}

	@Override
	public GregorianCalendar getLastModificationDate() {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(this.resource.getCreated());
		return c;
	}

	@Override
	public List<SecondaryType> getSecondaryTypes() {
		return Collections.emptyList();
	}

	@Override
	public List<ObjectType> findObjectType(String id) {
		return Collections.emptyList();
	}

	@Override
	public String getChangeToken() {
		return resource.getPublicKey();
	}

	@Override
	public AllowableActions getAllowableActions() {
		return new YandexAllowableActions();
	}

	@Override
	public boolean hasAllowableAction(Action action) {
		return true;
	}

	@Override
	public List<Relationship> getRelationships() {
		return Collections.emptyList();
	}

	@Override
	public Set<String> getPermissionsForPrincipal(String principalId) {
		return Collections.emptySet();
	}

	@Override
	public void delete(boolean allVersions) {
		delete();
	}

	@Override
	public CmisObject updateProperties(Map<String, ?> properties) {
		return this;
	}

	@Override
	public ObjectId updateProperties(Map<String, ?> properties, boolean refresh) {
		return this;
	}

	@Override
	public CmisObject updateProperties(Map<String, ?> properties, List<String> addSecondaryTypeIds,
			List<String> removeSecondaryTypeIds) {
		return this;
	}

	@Override
	public ObjectId updateProperties(Map<String, ?> properties, List<String> addSecondaryTypeIds,
			List<String> removeSecondaryTypeIds, boolean refresh) {
		return this;
	}

	@Override
	public ObjectId rename(String newName, boolean refresh) {
		return rename(newName);
	}

	@Override
	public List<Rendition> getRenditions() {
		return Collections.emptyList();
	}

	@Override
	public void applyPolicy(ObjectId... policyIds) {
	}

	@Override
	public void applyPolicy(ObjectId policyId, boolean refresh) {
	}

	@Override
	public void removePolicy(ObjectId... policyIds) {
	}

	@Override
	public void removePolicy(ObjectId policyId, boolean refresh) {
	}

	@Override
	public List<Policy> getPolicies() {
		return Collections.emptyList();
	}

	@Override
	public List<ObjectId> getPolicyIds() {
		return Collections.emptyList();
	}
	
	@Override
	public Acl getAcl() {
		return null;
	}
	
	@Override
	public Acl applyAcl(List<Ace> addAces, List<Ace> removeAces, AclPropagation aclPropagation) {
		return null;
	}

	@Override
	public Acl addAcl(List<Ace> addAces, AclPropagation aclPropagation) {
		return null;
	}

	@Override
	public Acl removeAcl(List<Ace> removeAces, AclPropagation aclPropagation) {
		return null;
	}

	@Override
	public Acl setAcl(List<Ace> aces) {
		return null;
	}

	@Override
	public List<CmisExtensionElement> getExtensions(ExtensionLevel level) {
		return Collections.emptyList();
	}

	@Override
	public <T> T getAdapter(Class<T> adapterInterface) {
		throw new UnsupportedOperationException();
	}

	@Override
	public long getRefreshTimestamp() {
		return timeRefresh;
	}

	@Override
	public void refreshIfOld(long durationInMillis) {
		
	}

}
