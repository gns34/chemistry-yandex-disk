package ru.gns34.chemistry.client.yandex.disk.spring;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.chemistry.opencmis.client.SessionFactoryFinder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

/**
 * @author Гончаров Никита
 * @since 1.0
 */
public class YandexDiskFactoryBean implements FactoryBean<Session>, InitializingBean, EnvironmentAware {

	private SessionFactory factory;
	private Environment environment;
	
	private Map<String, Session> instances = new ConcurrentHashMap<>();
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.factory = SessionFactoryFinder.find();
	}
	
	@Override
	public Class<?> getObjectType() {
		return Session.class;
	}

	@Override
	public Session getObject() throws Exception {
		final String user = environment.getProperty("yandex.user");
		final String token = environment.getProperty("yandex.token");
		final String key = user + token;
		
		Session session = this.instances.get(key);
		if (session != null) {
			return session;
		}
		
		Map<String, String> parameters = new HashMap<>();
		parameters.put(SessionParameter.USER, user);
		parameters.put(SessionParameter.OAUTH_ACCESS_TOKEN, token);
		
		session = factory.createSession(parameters);
		instances.put(key, session);
		
		return session;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	public void setEnvironment(Environment environment) {
		Assert.notNull(environment, "Environment must not be null");
		this.environment = environment;
	}
}
