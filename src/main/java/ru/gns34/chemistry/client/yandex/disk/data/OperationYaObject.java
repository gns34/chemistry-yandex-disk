/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yandex.disk.rest.YandexDiskClient;
import com.yandex.disk.rest.json.Link;
import com.yandex.disk.rest.json.Link.HttpStatus;
import com.yandex.disk.rest.json.Resource;

import ru.gns34.chemistry.client.yandex.disk.YandexDiskSessionImplRest;

/**
 * @author sbt-goncharov-ns
 *
 */
public abstract class OperationYaObject extends AbstractYaObject implements FileableCmisObject {

	private static final Logger LOGGER = LoggerFactory.getLogger(OperationYaObject.class);
	
	protected YandexDiskSessionImplRest session;
	
	public OperationYaObject(YandexDiskSessionImplRest session, Resource resource) {
		super(resource);
	}

	@Override
	public FileableCmisObject move(ObjectId sourceFolderId, ObjectId targetFolderId) {
		session.removeObjectFromCache(this);
		
		return null;
	}

	@Override
	public void delete() {
		session.removeObjectFromCache(this);
		this.session.deleteByPath(resource.getPath().getPath());
	}

	@Override
	public CmisObject rename(String newName) {
		if (newName == null || newName.equals(getName())) {
			return this;
		}
		try {
			String pathOrig = resource.getPath().getPath();
			String pathNew = YandexDiskClient.getParentPath(pathOrig) + newName;
			Link result = this.session.getClient().move(pathOrig, pathNew, true);
			if (!result.getHttpStatus().equals(HttpStatus.error)) {
				session.removeObjectFromCache(this);
				return this.session.getObjectByPath(pathNew);
			}
		} catch (Exception e) {
			LOGGER.error("Не удалось переименовать " + this + " в новое имя " + newName, e);
		}
		return this;
	}

	@Override
	public void refresh() {
		session.removeObjectFromCache(this);
	}

	@Override
	public FileableCmisObject move(ObjectId sourceFolderId, ObjectId targetFolderId, OperationContext context) {
		return move(sourceFolderId, targetFolderId);
	}

	@Override
	public List<Folder> getParents() {
		final String pathOrig = resource.getPath().getPath();
		final String pathNew = YandexDiskClient.getParentPath(pathOrig);
		final List<Folder> listPatents = Collections.emptyList();
		if (pathNew.equals(pathOrig)) {
			return listPatents;
		}
		CmisObject objectByPath = session.getObjectByPath(pathNew);
		if (objectByPath != null && BaseTypeId.CMIS_FOLDER.equals(objectByPath.getBaseTypeId())) {
			listPatents.add((Folder) objectByPath);
		}
		return listPatents;
	}

	@Override
	public List<Folder> getParents(OperationContext context) {
		return getParents();
	}

	@Override
	public List<String> getPaths() {
		List<String> paths = new ArrayList<>(1);
		paths.add(resource.getPath().getPath());
		return paths;
	}

	@Override
	public void addToFolder(ObjectId folderId, boolean allVersions) {
	}

	@Override
	public void removeFromFolder(ObjectId folderId) {
	}

}
