/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.datatype;

import java.util.Collections;
import java.util.Map;

import org.apache.chemistry.opencmis.commons.definitions.PropertyDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeMutability;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;

import ru.gns34.chemistry.client.yandex.disk.data.YaExtensionsData;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YaTypeDefinition extends YaExtensionsData implements TypeDefinition {

	private static final long serialVersionUID = 1L;
	
	private final String type;
	
	public YaTypeDefinition(String type) {
		this.type = type;
	}

	@Override
	public String getId() {
		return type;
	}

	@Override
	public String getLocalName() {
		return type;
	}

	@Override
	public String getLocalNamespace() {
		return type;
	}

	@Override
	public String getDisplayName() {
		return type;
	}

	@Override
	public String getQueryName() {
		return type;
	}

	@Override
	public String getDescription() {
		return type;
	}

	@Override
	public BaseTypeId getBaseTypeId() {
		return null;
	}

	@Override
	public String getParentTypeId() {
		return null;
	}

	@Override
	public Boolean isCreatable() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isFileable() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isQueryable() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean isFulltextIndexed() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean isIncludedInSupertypeQuery() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean isControllablePolicy() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean isControllableAcl() {
		return Boolean.FALSE;
	}

	@Override
	public Map<String, PropertyDefinition<?>> getPropertyDefinitions() {
		return Collections.emptyMap();
	}

	@Override
	public TypeMutability getTypeMutability() {
		return YaTypeMutability.getInstance();
	}


}
