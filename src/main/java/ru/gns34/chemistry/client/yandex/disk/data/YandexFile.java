/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.io.OutputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.DocumentType;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.ContentStreamHash;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;

import com.yandex.disk.rest.json.Resource;

import ru.gns34.chemistry.client.yandex.disk.YandexDiskSessionImplRest;
import ru.gns34.chemistry.client.yandex.disk.datatype.YaFileType;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YandexFile extends OperationYaObject implements Document {

	public YandexFile(YandexDiskSessionImplRest session, Resource resource) {
		super(session, resource);
	}

	@Override
	public BaseTypeId getBaseTypeId() {
		return BaseTypeId.CMIS_DOCUMENT;
	}

	@Override
	public ObjectType getBaseType() {
		return new YaFileType();
	}

	@Override
	public ObjectType getType() {
		return new YaFileType();
	}

	@Override
	public Boolean isImmutable() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isLatestVersion() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isMajorVersion() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isLatestMajorVersion() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isPrivateWorkingCopy() {
		// TODO Auto-generated method stub
		return Boolean.FALSE;
	}

	@Override
	public String getVersionLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getVersionSeriesId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isVersionSeriesCheckedOut() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getVersionSeriesCheckedOutBy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getVersionSeriesCheckedOutId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCheckinComment() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getContentStreamLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getContentStreamMimeType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContentStreamFileName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContentStreamId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContentStreamHash> getContentStreamHashes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLatestAccessibleStateId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentType getDocumentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isVersionable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Boolean isVersionSeriesPrivateWorkingCopy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllVersions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ContentStream getContentStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentStream getContentStream(BigInteger offset, BigInteger length) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentStream getContentStream(String streamId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentStream getContentStream(String streamId, BigInteger offset, BigInteger length) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContentUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContentUrl(String streamId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document setContentStream(ContentStream contentStream, boolean overwrite) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId setContentStream(ContentStream contentStream, boolean overwrite, boolean refresh) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document appendContentStream(ContentStream contentStream, boolean isLastChunk) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId appendContentStream(ContentStream contentStream, boolean isLastChunk, boolean refresh) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document deleteContentStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId deleteContentStream(boolean refresh) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream createOverwriteOutputStream(String filename, String mimeType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream createOverwriteOutputStream(String filename, String mimeType, int bufferSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream createAppendOutputStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream createAppendOutputStream(int bufferSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId checkOut() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cancelCheckOut() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ObjectId checkIn(boolean major, Map<String, ?> properties, ContentStream contentStream,
			String checkinComment, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId checkIn(boolean major, Map<String, ?> properties, ContentStream contentStream,
			String checkinComment) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document getObjectOfLatestVersion(boolean major) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document getObjectOfLatestVersion(boolean major, OperationContext context) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Document> getAllVersions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Document> getAllVersions(OperationContext context) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document copy(ObjectId targetFolderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document copy(ObjectId targetFolderId, Map<String, ?> properties, VersioningState versioningState,
			List<Policy> policies, List<Ace> addACEs, List<Ace> removeACEs, OperationContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
