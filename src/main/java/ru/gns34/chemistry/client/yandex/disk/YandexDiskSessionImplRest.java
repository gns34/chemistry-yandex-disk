package ru.gns34.chemistry.client.yandex.disk;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.chemistry.opencmis.client.api.ChangeEvent;
import org.apache.chemistry.opencmis.client.api.ChangeEvents;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectFactory;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.QueryStatement;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.Tree;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.data.BulkUpdateObjectIdAndChangeToken;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships;
import org.apache.chemistry.opencmis.commons.enums.RelationshipDirection;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.spi.CmisBinding;

import com.yandex.disk.rest.YandexDiskClient;
import com.yandex.disk.rest.json.ApiVersion;
import com.yandex.disk.rest.json.Resource;

import ru.gns34.chemistry.client.yandex.disk.data.YandexFolder;
import ru.gns34.chemistry.client.yandex.disk.data.YandexRepositoryInfo;

/**
 * @author Гончаров Никита
 * @since 1.0
 */
public class YandexDiskSessionImplRest implements Session {

	private static final long serialVersionUID = 1L;
	//private static final Logger LOGGER = LoggerFactory.getLogger(YandexDiskSessionImplRest.class);
	private static final String STRING = "";
	
	private final YandexDiskClient client;
	private final Map<String, String> parameters;
	private final Resource root;


	public YandexDiskSessionImplRest(YandexDiskClient client, Map<String, String> parameters, Resource resource) {
		this.client = client;
		this.parameters = parameters;
		this.root = resource;
	}

	/**
	 * @return the Native client yandex disk
	 */
	public YandexDiskClient getClient() {
		return client;
	}

	@Override
	public void clear() {
		// TODO Почистить всю КЕШ объектов
		
	}

	@Override
	public CmisBinding getBinding() {
		// Не поддеживается
		return null;
	}

	@Override
	public Map<String, String> getSessionParameters() {
		return Collections.unmodifiableMap(this.parameters);
	}

	@Override
	public OperationContext getDefaultContext() {
		// Не поддеживается
		return null;
	}

	@Override
	public void setDefaultContext(OperationContext context) {
		// Не поддеживается
		
	}

	@Override
	public OperationContext createOperationContext() {
		// Не поддеживается
		return null;
	}

	@Override
	public OperationContext createOperationContext(Set<String> filter, boolean includeAcls,
			boolean includeAllowableActions, boolean includePolicies, IncludeRelationships includeRelationships,
			Set<String> renditionFilter, boolean includePathSegments, String orderBy, boolean cacheEnabled,
			int maxItemsPerPage) {
		// Не поддеживается
		return null;
	}

	@Override
	public ObjectId createObjectId(String id) {
		// Не поддеживается
		return null;
	}

	@Override
	public Locale getLocale() {
		return Locale.getDefault();
	}

	@Override
	public RepositoryInfo getRepositoryInfo() {
		YandexRepositoryInfo info = new YandexRepositoryInfo();
		ApiVersion apiVersion = this.client.safeGetApiVersion();
		info.setClientUri(this.client.getClientUri());
		info.setProductVersion(apiVersion.getApiVersion());
		return info;
	}

	@Override
	public ObjectFactory getObjectFactory() {
		return null;
	}

	@Override
	public ObjectType getTypeDefinition(String typeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectType getTypeDefinition(String typeId, boolean useCache) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemIterable<ObjectType> getTypeChildren(String typeId, boolean includePropertyDefinitions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tree<ObjectType>> getTypeDescendants(String typeId, int depth, boolean includePropertyDefinitions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectType createType(TypeDefinition type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectType updateType(TypeDefinition type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteType(String typeId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Folder getRootFolder() {
		return new YandexFolder(this, root);
	}

	@Override
	public Folder getRootFolder(OperationContext context) {
		return getRootFolder();
	}

	@Override
	public ItemIterable<Document> getCheckedOutDocs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemIterable<Document> getCheckedOutDocs(OperationContext context) {
		return getCheckedOutDocs();
	}

	@Override
	public CmisObject getObject(ObjectId objectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmisObject getObject(ObjectId objectId, OperationContext context) {
		return getObject(objectId);
	}

	@Override
	public CmisObject getObject(String objectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmisObject getObject(String objectId, OperationContext context) {
		return getObject(objectId);
	}

	@Override
	public CmisObject getObjectByPath(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmisObject getObjectByPath(String path, OperationContext context) {
		return getObjectByPath(path);
	}

	@Override
	public CmisObject getObjectByPath(String parentPath, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmisObject getObjectByPath(String parentPath, String name, OperationContext context) {
		return getObjectByPath(parentPath);
	}

	@Override
	public Document getLatestDocumentVersion(ObjectId objectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document getLatestDocumentVersion(ObjectId objectId, OperationContext context) {
		return getLatestDocumentVersion(objectId);
	}

	@Override
	public Document getLatestDocumentVersion(ObjectId objectId, boolean major, OperationContext context) {
		return getLatestDocumentVersion(objectId);
	}

	@Override
	public Document getLatestDocumentVersion(String objectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document getLatestDocumentVersion(String objectId, OperationContext context) {
		return getLatestDocumentVersion(objectId);
	}

	@Override
	public Document getLatestDocumentVersion(String objectId, boolean major, OperationContext context) {
		return getLatestDocumentVersion(objectId);
	}

	@Override
	public boolean exists(ObjectId objectId) {
		return exists(objectId.getId());
	}

	@Override
	public boolean exists(String objectId) {
		return existsPath(objectId);
	}

	@Override
	public boolean existsPath(String path) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean existsPath(String parentPath, String name) {
		return existsPath(parentPath + '/' + name);
	}

	@Override
	public void removeObjectFromCache(ObjectId objectId) {
		removeObjectFromCache(objectId.getId());
	}

	@Override
	public void removeObjectFromCache(String objectId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ItemIterable<QueryResult> query(String statement, boolean searchAllVersions) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ItemIterable<QueryResult> query(String statement, boolean searchAllVersions, OperationContext context) {
		return query(statement, searchAllVersions);
	}

	@Override
	public ItemIterable<CmisObject> queryObjects(String typeId, String where, boolean searchAllVersions,
			OperationContext context) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public QueryStatement createQueryStatement(String statement) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public QueryStatement createQueryStatement(Collection<String> selectPropertyIds, Map<String, String> fromTypes,
			String whereClause, List<String> orderByPropertyIds) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ChangeEvents getContentChanges(String changeLogToken, boolean includeProperties, long maxNumItems) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ChangeEvents getContentChanges(String changeLogToken, boolean includeProperties, long maxNumItems,
			OperationContext context) {
		return getContentChanges(changeLogToken, includeProperties, maxNumItems);
	}

	@Override
	public ItemIterable<ChangeEvent> getContentChanges(String changeLogToken, boolean includeProperties) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ItemIterable<ChangeEvent> getContentChanges(String changeLogToken, boolean includeProperties,
			OperationContext context) {
		return getContentChanges(changeLogToken, includeProperties);
	}

	@Override
	public String getLatestChangeLogToken() {
		return STRING;
	}

	@Override
	public ObjectId createDocument(Map<String, ?> properties, ObjectId folderId, ContentStream contentStream,
			VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces) {
		return createDocument(properties, folderId, contentStream, versioningState);
	}

	@Override
	public ObjectId createDocument(Map<String, ?> properties, ObjectId folderId, ContentStream contentStream,
			VersioningState versioningState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId createDocumentFromSource(ObjectId source, Map<String, ?> properties, ObjectId folderId,
			VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces) {
		return createDocumentFromSource(source, properties, folderId, versioningState);
	}

	@Override
	public ObjectId createDocumentFromSource(ObjectId source, Map<String, ?> properties, ObjectId folderId,
			VersioningState versioningState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId createFolder(Map<String, ?> properties, ObjectId folderId, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces) {
		return createFolder(properties, folderId);
	}

	@Override
	public ObjectId createFolder(Map<String, ?> properties, ObjectId folderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId createPath(String newPath, String typeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId createPath(ObjectId startFolderId, String newPath, String typeId) {
		return createPath(startFolderId.getId() + '/' + newPath, typeId);
	}

	@Override
	public ObjectId createPath(String newPath, Map<String, ?> properties) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectId createPath(ObjectId startFolderId, String newPath, Map<String, ?> properties) {
		return createPath(startFolderId.getId() + '/' + newPath, properties);
	}

	@Override
	public ObjectId createPath(ObjectId startFolderId, String newPath, Map<String, ?> properties, List<Policy> policies,
			List<Ace> addAces, List<Ace> removeAces) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createPolicy(Map<String, ?> properties, ObjectId folderId, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createPolicy(Map<String, ?> properties, ObjectId folderId) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createItem(Map<String, ?> properties, ObjectId folderId, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createItem(Map<String, ?> properties, ObjectId folderId) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createRelationship(Map<String, ?> properties, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectId createRelationship(Map<String, ?> properties) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public ItemIterable<Relationship> getRelationships(ObjectId objectId, boolean includeSubRelationshipTypes,
			RelationshipDirection relationshipDirection, ObjectType type, OperationContext context) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public List<BulkUpdateObjectIdAndChangeToken> bulkUpdateProperties(List<CmisObject> objects,
			Map<String, ?> properties, List<String> addSecondaryTypeIds, List<String> removeSecondaryTypeIds) {
		// Не поддеживается
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(ObjectId objectId) {
		deleteByPath(objectId.getId());
	}

	@Override
	public void delete(ObjectId objectId, boolean allVersions) {
		delete(objectId);
	}

	@Override
	public void deleteByPath(String path) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteByPath(String parentPath, String name) {
		deleteByPath(parentPath + '/' + name);
	}

	@Override
	public void deleteByPath(String path, boolean allVersions) {
		deleteByPath(path);
	}

	@Override
	public List<String> deleteTree(ObjectId folderId, boolean allVersions, UnfileObject unfile,
			boolean continueOnFailure) {
		deleteByPath(folderId.getId());
		return Collections.emptyList();
	}

	@Override
	public List<String> deleteTreebyPath(String parentPath, String name, boolean allVersions, UnfileObject unfile,
			boolean continueOnFailure) {
		return deleteTreebyPath(parentPath + '/' + name, allVersions, unfile, continueOnFailure);
	}

	@Override
	public List<String> deleteTreebyPath(String path, boolean allVersions, UnfileObject unfile,
			boolean continueOnFailure) {
		deleteByPath(path);
		return Collections.emptyList();
	}

	@Override
	public ContentStream getContentStream(ObjectId docId) {
		return getContentStreamByPath(docId.getId());
	}

	@Override
	public ContentStream getContentStream(ObjectId docId, String streamId, BigInteger offset, BigInteger length) {
		return getContentStream(docId);
	}

	@Override
	public ContentStream getContentStreamByPath(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContentStream getContentStreamByPath(String path, String streamId, BigInteger offset, BigInteger length) {
		return getContentStreamByPath(path);
	}

	@Override
	public Acl getAcl(ObjectId objectId, boolean onlyBasicPermissions) {
		return null;
	}

	@Override
	public Acl applyAcl(ObjectId objectId, List<Ace> addAces, List<Ace> removeAces, AclPropagation aclPropagation) {
		return null;
	}

	@Override
	public Acl setAcl(ObjectId objectId, List<Ace> aces) {
		return null;
	}

	@Override
	public void applyPolicy(ObjectId objectId, ObjectId... policyIds) {
		
	}

	@Override
	public void removePolicy(ObjectId objectId, ObjectId... policyIds) {
	}

}
