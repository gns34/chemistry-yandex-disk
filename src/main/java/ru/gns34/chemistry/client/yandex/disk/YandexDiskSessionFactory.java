package ru.gns34.chemistry.client.yandex.disk;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.commons.SessionParameter;

import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.ResourcesArgs;
import com.yandex.disk.rest.YandexDiskClient;
import com.yandex.disk.rest.exceptions.http.UnauthorizedException;
import com.yandex.disk.rest.json.Resource;

/**
 * @author Гончаров Никита
 * @since 0.0.1
 */
public class YandexDiskSessionFactory implements SessionFactory {

	private YandexDiskSessionFactory() {
	}
	
	public static SessionFactory newInstance() {
		return new YandexDiskSessionFactory();
	}
	
	@Override
	public Session createSession(Map<String, String> parameters) {
		final String user = parameters.get(SessionParameter.USER);
		String token = parameters.get(SessionParameter.OAUTH_ACCESS_TOKEN);
		if (user == null || user.trim().length() == 0) {
			throw new IllegalArgumentException("Логин отсутствует");
		}
		if (token == null || token.trim().length() == 0) {
			token = parameters.get(SessionParameter.PASSWORD);
		}
		if (token == null || token.trim().length() == 0) {
			throw new IllegalArgumentException("Токен отсутствует");
		}
		try {
			Credentials credentials = new Credentials(user, token);
			YandexDiskClient client = new YandexDiskClient(credentials);
			
			ResourcesArgs.Builder builder = new ResourcesArgs.Builder();
			builder.setPath("disk:/");
			Resource resource = client.getResources(builder.build());
			YandexDiskSessionImplRest session = new YandexDiskSessionImplRest(client, parameters, resource); 
			return session;
		} catch (UnauthorizedException e) {
			throw new IllegalArgumentException("Не правильный токен для логина " + user + " или токен при подключении", e);
		} catch (Exception e) {
			throw new IllegalArgumentException("Внутреняя ошибка создания сессии подключения", e);
		}
	}

	@Override
	public List<Repository> getRepositories(Map<String, String> parameters) {
		return Collections.unmodifiableList(Collections.emptyList());
	}

}
