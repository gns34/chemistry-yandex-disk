/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.datatype;

import org.apache.chemistry.opencmis.client.api.DocumentType;
import org.apache.chemistry.opencmis.commons.enums.ContentStreamAllowed;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YaFileType extends AbstractYaObjectType implements DocumentType {

	private static final long serialVersionUID = 1L;

	public YaFileType() {
		super("file");
	}

	@Override
	public Boolean isVersionable() {
		return Boolean.FALSE;
	}

	@Override
	public ContentStreamAllowed getContentStreamAllowed() {
		return ContentStreamAllowed.REQUIRED;
	}


}
