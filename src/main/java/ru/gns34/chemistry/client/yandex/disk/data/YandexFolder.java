/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.FolderType;
import org.apache.chemistry.opencmis.client.api.Item;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.client.api.Tree;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;

import com.yandex.disk.rest.YandexDiskClient;
import com.yandex.disk.rest.json.Resource;
import com.yandex.disk.rest.json.ResourceList;

import ru.gns34.chemistry.client.yandex.disk.YandexDiskSessionImplRest;
import ru.gns34.chemistry.client.yandex.disk.datatype.YaFileType;
import ru.gns34.chemistry.client.yandex.disk.datatype.YaFolderType;
import ru.gns34.chemistry.client.yandex.disk.datatype.YaObjectIterable;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YandexFolder extends OperationYaObject implements Folder {

	
	public YandexFolder(YandexDiskSessionImplRest session, Resource resource) {
		super(session, resource);
	}

	@Override
	public BaseTypeId getBaseTypeId() {
		return BaseTypeId.CMIS_FOLDER;
	}

	@Override
	public ObjectType getBaseType() {
		return new YaFolderType();
	}

	@Override
	public ObjectType getType() {
		return new YaFolderType();
	}

	@Override
	public String getParentId() {
		final String pathOrig = resource.getPath().getPath();
		final String pathNew = YandexDiskClient.getParentPath(pathOrig);
		if (pathNew.equals(pathOrig)) {
			return null;
		}
		return pathNew;
	}

	@Override
	public List<ObjectType> getAllowedChildObjectTypes() {
		ArrayList<ObjectType> objects = new ArrayList<>(2);
		objects.add(new YaFolderType());
		objects.add(new YaFileType());
		return objects;
	}

	@Override
	public FolderType getFolderType() {
		return new YaFolderType();
	}

	@Override
	public Document createDocument(Map<String, ?> properties, ContentStream contentStream,
			VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces,
			OperationContext context) {
		return createDocument(properties, contentStream, versioningState);
	}

	@Override
	public Document createDocument(Map<String, ?> properties, ContentStream contentStream,
			VersioningState versioningState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document createDocumentFromSource(ObjectId source, Map<String, ?> properties,
			VersioningState versioningState, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces,
			OperationContext context) {
		return createDocumentFromSource(source, properties, versioningState);
	}

	@Override
	public Document createDocumentFromSource(ObjectId source, Map<String, ?> properties,
			VersioningState versioningState) {
		return null;
	}

	@Override
	public Folder createFolder(Map<String, ?> properties, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces, OperationContext context) {
		return createFolder(properties);
	}

	@Override
	public Folder createFolder(Map<String, ?> properties) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Policy createPolicy(Map<String, ?> properties, List<Policy> policies, List<Ace> addAces,
			List<Ace> removeAces, OperationContext context) {
		return null;
	}

	@Override
	public Item createItem(Map<String, ?> properties) {
		return null;
	}

	@Override
	public Item createItem(Map<String, ?> properties, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces,
			OperationContext context) {
		return null;
	}

	@Override
	public Policy createPolicy(Map<String, ?> properties) {
		return null;
	}

	@Override
	public List<String> deleteTree(boolean allversions, UnfileObject unfile, boolean continueOnFailure) {
		delete();
		return Collections.emptyList();
	}

	@Override
	public List<Tree<FileableCmisObject>> getFolderTree(int depth) {
		return null;
	}

	@Override
	public List<Tree<FileableCmisObject>> getFolderTree(int depth, OperationContext context) {
		return getFolderTree(depth);
	}

	@Override
	public List<Tree<FileableCmisObject>> getDescendants(int depth) {
		return null;
	}

	@Override
	public List<Tree<FileableCmisObject>> getDescendants(int depth, OperationContext context) {
		return getDescendants(depth);
	}

	@Override
	public ItemIterable<CmisObject> getChildren() {
		ResourceList resourceList = this.resource.getResourceList();
		return new YaObjectIterable(this.session, resourceList.getItems());
	}

	@Override
	public ItemIterable<CmisObject> getChildren(OperationContext context) {
		return getChildren();
	}

	@Override
	public boolean isRootFolder() {
		return "/".equals(this.resource.getPath().getPath());
	}

	@Override
	public Folder getFolderParent() {
		if (isRootFolder()) {
			return null;
		}
		Resource folderParent = this.session.getClient().getFolderParent(this.resource.getPath());
		return new YandexFolder(session, folderParent);
	}

	@Override
	public String getPath() {
		return this.resource.getPath().getPath();
	}

	@Override
	public ItemIterable<Document> getCheckedOutDocs() {
		return null;
	}

	@Override
	public ItemIterable<Document> getCheckedOutDocs(OperationContext context) {
		return getCheckedOutDocs();
	}


}
