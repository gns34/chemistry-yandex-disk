/**
 * 
 */
package ru.gns34.chemistry.client.yandex.disk.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.enums.Action;

/**
 * @author sbt-goncharov-ns
 *
 */
public class YandexAllowableActions extends YaExtensionsData implements AllowableActions {

	@Override
	public Set<Action> getAllowableActions() {
		return new HashSet<>(Arrays.asList(Action.values()));
	}

}
