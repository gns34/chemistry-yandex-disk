/**
 * 
 */
package ru.gns34;

import org.apache.chemistry.opencmis.client.api.Session;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import ru.gns34.chemistry.client.yandex.disk.spring.YandexDiskFactoryBean;

/**
 * @author sbt-goncharov-ns
 *
 */
public class FactoryTest {

	@Test
	public void run() {
		@SuppressWarnings("resource")
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.getEnvironment().setActiveProfiles("test");
        context.load("classpath:testSpringContext.xml");
        context.refresh();
        
        YandexDiskFactoryBean yandexFa = context.getBean(YandexDiskFactoryBean.class);
        System.out.println("A " + yandexFa);
        
        Object yandexName = context.getBean("yandexDiskFactory");
        System.out.println("Get bean yandex by name: " + yandexName);

        Object yandexClass = context.getBean(Session.class);
        System.out.println("Get bean yandex by class:  " + yandexClass);
        Assert.assertSame(yandexName, yandexClass);
        
        System.getProperties().put("yandex.user", "1");
        System.getProperties().put("yandex.token", "2");
        Session yandexSession1 = context.getBean(Session.class);
        Session yandexSession11 = context.getBean(Session.class);
        Assert.assertSame(yandexSession1, yandexSession11);
        
        System.getProperties().put("yandex.user", "3");
        System.getProperties().put("yandex.token", "4");
        Session yandexSession2 = context.getBean(Session.class);
        
        Assert.assertNotSame(yandexSession1, yandexSession2);
	}
}
