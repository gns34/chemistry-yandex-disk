/**
 * 
 */
package ru.gns34;

import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.junit.Test;

import ru.gns34.chemistry.client.yandex.disk.YandexDiskSessionFactory;

/**
 * @author Гончаров Никита
 * @since 0.0.1
 */
public class ClientAPITest {

	@Test
	public void run() {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(SessionParameter.REPOSITORY_ID, "A1");
		parameters.put(SessionParameter.USER, "yadiskrestapitest");
		parameters.put(SessionParameter.PASSWORD, "4dc977dfd8024be1bba27a5fefc1c1d0");

		// create the session
		Session session = YandexDiskSessionFactory.newInstance().createSession(parameters);

		// get repository info
		RepositoryInfo repInfo = session.getRepositoryInfo();
		System.out.println("Repository name: " + repInfo.getName());

		// get root folder and its path
		Folder rootFolder = session.getRootFolder();
		String path = rootFolder.getPath();
		System.out.println("Root folder path: " + path);

		// list root folder children
		ItemIterable<CmisObject> children = rootFolder.getChildren();
		for (CmisObject object : children) {
		  System.out.println("---------------------------------");
		  System.out.println("  Id:       " + object.getId());
		  System.out.println("  Name:         " + object.getName());
		  System.out.println("  Base Type:    " + object.getBaseTypeId());
		  System.out.println("  Property 'bla':   " +
		  object.getPropertyValue("bla"));

		  ObjectType type = object.getType();
		  System.out.println("  Type Id:      " + type.getId());
		  System.out.println("  Type Name:    " + type.getDisplayName());
		  System.out.println("  Type Query Name:  " + type.getQueryName());

		  AllowableActions actions = object.getAllowableActions();
		  System.out.println("  canGetProperties: " +
		    actions.getAllowableActions().contains(Action.CAN_GET_PROPERTIES));
		  System.out.println("  canDeleteObject:  " +
		    actions.getAllowableActions().contains(Action.CAN_DELETE_OBJECT));
		}

		// get an object
		ObjectId objectId = session.createObjectId("100");
		CmisObject object = session.getObject(objectId);

		if (object instanceof Folder) {
		  Folder folder = (Folder) object;
		  System.out.println("Is root folder: " + folder.isRootFolder());
		}

		if (object instanceof Document) {
		  Document document = (Document) object;
		  ContentStream content = document.getContentStream();
		  System.out.println("Document MIME type: " + content.getMimeType());
		}
	}
}
